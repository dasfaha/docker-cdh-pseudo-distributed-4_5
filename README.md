  Cloudera Distribution Hadoop 4.5 within Docker
==============

1. Enter dockerfiles.git and run docker installer.
$> ./local_env.sh*
Note: local_env.sh also creates 3 folders in your filesystem that are accessible to Docker container

1. Build the container (it will take a minute or two):
$> ./build.sh

1. Run the container:
$> ./run.sh

1. Once in the container, perform ONE_TIME_ONLY:
#> cd root
#> ./hdfs_format.sh

1. Start hadoop cluster:
#> ./hadoop_pseudo_start.sh

1. Perform ONE_TIME_ONLY:
#> ./hdfs_init.sh

1. Start/Stop:
#> ./hadoop_pseudo_stop.sh
#> ./clear_hadoop_logs.sh
#> ./hadoop_pseudo_start.sh

1. Enjoy your cluster:
#> su - ;  # emulates login and hence - reads all env variables
#> hdfs dfs -ls -R /
#> hbase shell
        status 'simple'
#> pig

Next folders are created in your FS and accessible by container:
/var/hstation/dfs              HDFS. No manual intervention needed
/var/hstation/workspace        In/Out folder to exchange data to/from container
/var/hstation/logs             To contain Hadoop logs (will be utilized later)


Test hadoop mapreduce job:
--------
#> hdfs dfs -mkdir test_input
#> hdfs dfs -put /etc/hadoop/conf/*.xml test_input
#> hfds dfs -ls test_input
#> hadoop jar /usr/lib/hadoop-mapreduce/hadoop-mapreduce-examples.jar grep input output23 'dfs[a-z.]+'
#> hdfs dfs -ls output23
#> hdfs dfs -cat output23/part-r-00000


Exposed ports
-------

1. Find out your container IP:
#> domainname -i
let's assume it is 172.17.0.XX

1. Navigate in your browser to:
http://172.17.0.XX:8088/cluster for Resource Manager

1. Navigate to:
http://172.17.0.XX:19888/jobhistory for Job History

1. Navigate to:
http://172.17.0.XX:50070 for HDFS Name Node

1. Navigate to:
http://172.17.0.XX:60010 for HBase Master

1. Navigate to:
http://172.17.0.XX:8042/node for Yarn Node Manager


