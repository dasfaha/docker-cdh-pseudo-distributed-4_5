#!/bin/bash

# system-wide structure
su - hdfs -c "hadoop fs -mkdir /tmp"
su - hdfs -c "hadoop fs -chmod -R 1777 /tmp"

su - hdfs -c "hadoop fs -mkdir /tmp/hadoop-yarn/staging"
su - hdfs -c "hadoop fs -chmod -R 1777 /tmp/hadoop-yarn/staging"

su - hdfs -c "hadoop fs -mkdir /tmp/hadoop-yarn/staging/history/done_intermediate"
su - hdfs -c "hadoop fs -chmod -R 1777 /tmp/hadoop-yarn/staging/history/done_intermediate"

su - hdfs -c "hadoop fs -chown -R mapred:mapred /tmp/hadoop-yarn/staging"

su - hdfs -c "hadoop fs -mkdir /var/log/hadoop-yarn"
su - hdfs -c "hadoop fs -chown yarn:mapred /var/log/hadoop-yarn"

# user-specific structure
su - hdfs -c "hadoop fs -mkdir /user/bohdan"
su - hdfs -c "hadoop fs -chown bohdan /user/bohdan"

su - hdfs -c "hadoop fs -mkdir /user/history"
su - hdfs -c "hadoop fs -chmod -R 1777 /user/history"

su - hdfs -c "hadoop fs -mkdir /user/yarn"
su - hdfs -c "hadoop fs -chown yarn /user/yarn"

# HBase-specific structure
su - hdfs -c "hadoop fs -mkdir /hbase"
su - hdfs -c "hadoop fs -chown hbase /hbase"