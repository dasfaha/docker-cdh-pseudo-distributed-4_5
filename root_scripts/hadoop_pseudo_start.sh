#!/bin/bash

set -x

# Create logging directories if needed:
mkdir -p --mode=777 /hlogs/hadoop-0.20-mapreduce
mkdir -p --mode=777 /hlogs/hadoop-hdfs
mkdir -p --mode=777 /hlogs/hadoop-mapreduce
mkdir -p --mode=777 /hlogs/hadoop-yarn
mkdir -p --mode=777 /hlogs/hbase
mkdir -p --mode=777 /hlogs/pig
mkdir -p --mode=777 /hlogs/zookeeper

# Removing existing logging directories
rm -fR /var/log/hadoop-0.20-mapreduce
rm -fR /var/log/hadoop-hdfs
rm -fR /var/log/hadoop-mapreduce
rm -fR /var/log/hadoop-yarn
rm -fR /var/log/hbase
rm -fR /var/log/pig
rm -fR /var/log/zookeeper

# Link log folders to the outside filesystem
ln -s /hlogs/hadoop-0.20-mapreduce /var/log/hadoop-0.20-mapreduce
ln -sf /hlogs/hadoop-hdfs /var/log/hadoop-hdfs
ln -s /hlogs/hadoop-mapreduce /var/log/hadoop-mapreduce
ln -s /hlogs/hadoop-yarn /var/log/hadoop-yarn
ln -s /hlogs/hbase /var/log/hbase
ln -s /hlogs/pig /var/log/pig
ln -s /hlogs/zookeeper /var/log/zookeeper

# HDFS section
for x in `cd /etc/init.d ; ls hadoop-hdfs-*` ; do service $x start ; done

# YARN section
service hadoop-yarn-resourcemanager start
service hadoop-yarn-nodemanager start
service hadoop-mapreduce-historyserver start

# HBase section
service zookeeper-server start
service hbase-master start
service hbase-regionserver start

$JAVA_HOME/bin/jps
