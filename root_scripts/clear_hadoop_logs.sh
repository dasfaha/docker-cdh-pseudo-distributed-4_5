#!/bin/bash

rm -r -f /var/log/hbase/*
rm -r -f /var/log/hadoop-mapreduce/*
rm -r -f /var/log/hadoop-hdfs/*
rm -r -f /var/log/hadoop-yarn/*
rm -r -f /var/log/zookeeper/*
